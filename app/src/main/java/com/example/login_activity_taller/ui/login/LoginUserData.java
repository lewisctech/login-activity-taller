package com.example.login_activity_taller.ui.login;

/**
 * Class for data model
 */
public class LoginUserData {
    private String activity;
    private String course;
    private String emotion;
    private float emotion_level;
    private String location;
    private String time;
    private String user_id;
    // Constructor
    public LoginUserData(String activity, String course, String emotion,
                         float emotion_level, String location, String time, String user_id) {

        this.activity = activity;
        this.course = course;
        this.emotion = emotion;
        this.emotion_level = emotion_level;
        this.location = location;
        this.time = time;
        this.user_id = user_id;
    }


    public String getActivity(){ return activity; }
    public String getCourse(){ return course; }
    public String getEmotion(){ return emotion; }
    public float getEmotionLevel() { return emotion_level; }
    public String getLocation(){ return location; }
    public String getTime(){ return time; }
    public String getUser_id(){ return user_id; }


    public void dataUserPrint(){
        System.out.println(activity);
        System.out.println(course);
        System.out.println(emotion);
        System.out.println(emotion_level);
        System.out.println(location);
        System.out.println(time);
        System.out.println(user_id);

    }
}